import json
import os
import random
import sys
from pathlib import Path
from spacy.util import compounding, minibatch

import numpy
import spacy
import tqdm

if not spacy.__version__.startswith("2."):
    print("spacy version must be 2.X.X for the training. "
          "If you want to user a recent version of spacy, you first need to convert the dataset accordingly.")
    sys.exit(1)


def train(input_data_path: str, model_save_path: str, hyperparams_path: str = None):
    """
    The function to execute the training.

    :param input_data_path: [str], input directory path where all the training file(s) reside in
    :param model_save_path: [str], directory path to save your model(s)
    :param hyperparams_path: [optional[str], default=None], input path to hyperparams json file.
    Example:
        {
            "max_leaf_nodes": 10,
            "n_estimators": 200
        }
    """
    print(f"Started training with input data path ({input_data_path}), model output path ({model_save_path}), "
          f"hyperparameters file path ({hyperparams_path})")
    spacy_model = "en_core_web_lg"

    if hyperparams_path:
        hyperparameters = json.loads(Path(hyperparams_path).read_bytes())
        if not hyperparameters:
            raise Exception(f"Failed to load hyper parameters from path: {hyperparams_path}")
        print(f"Loaded hyper parameters from {hyperparams_path}: {hyperparameters}")
    else:
        hyperparameters = dict()

    dropout = float(hyperparameters.get("dropout", "0.2"))
    n_iter = int(hyperparameters.get("n_iter", "10"))
    seed = int(hyperparameters.get("seed", "0"))

    random.seed(seed)
    numpy.random.seed(seed)

    if spacy_model.startswith("blank:"):
        nlp = spacy.blank(spacy_model.replace("blank:", ""))
    else:
        nlp = spacy.load(spacy_model)

    nlp.add_pipe(nlp.create_pipe("textcat", config={"exclusive_classes": False}), last=True)
    pipe = nlp.get_pipe("textcat")
    pipe.add_label("Contract")

    disabled = nlp.disable_pipes([p for p in nlp.pipe_names if p != "textcat"])
    optimizer = nlp.begin_training(component_cfg={'textcat': {'exclusive_classes': False}})
    batch_size = compounding(1.0, 16.0, 1.001)
    best_scores = None
    best_model = None

    input_data_folder = Path(input_data_path)
    input_files = get_filenames(input_data_folder)
    full_dataset = None
    for file in input_files:
        print(f'Reading file: {file}')
        with open(file, 'r') as f:
            full_dataset = json.load(f)

    eval_split = 0.5
    random.shuffle(full_dataset)
    test_dataset = full_dataset[:int(len(full_dataset) * eval_split)]
    train_dataset = full_dataset[len(test_dataset):]

    baseline = nlp.evaluate(test_dataset)

    print(f"Baseline F1 score: {baseline.textcat_score};")
    print(f"iter \t loss \t F1")

    for i in range(n_iter):
        random.shuffle(train_dataset)
        losses = {}
        data = tqdm.tqdm(train_dataset, leave=False, desc=f"{i + 1}")
        for batch in minibatch(data, size=batch_size):
            docs, annots = zip(*batch)
            nlp.update(docs, annots, drop=dropout, losses=losses)
        with nlp.use_params(optimizer.averages):
            scores = nlp.evaluate(test_dataset)
            print(f"{i} \t {losses['textcat']} \t {scores.textcat_score}")
            if not best_scores or scores.textcat_score > best_scores.textcat_score:
                best_scores = scores
                best_model = pipe.to_bytes(exclude=["vocab"])

    print(f"Best F1 score: {best_scores.textcat_score};")

    pipe.from_bytes(best_model)
    if disabled:
        disabled.restore()
    output_path = os.path.join(model_save_path, "is_contract_budget")
    print(f"Writing model to {str(output_path)}")
    nlp.to_disk(output_path)


def get_filenames(data_folder):
    return [os.path.join(data_folder, file) for file in os.listdir(data_folder)]
